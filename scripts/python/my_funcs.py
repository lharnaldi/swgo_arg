#!/usr/bin/python3
# -*- encoding: utf-8 -*-

import os
import numpy as np
#First, we build a list of the file names. isfile() is used to skip directories;
#it can be omitted if directories should be included. Then, we sort the list
#in-place, using the modify date as the key.
#https://stackoverflow.com/questions/168409/how-do-you-get-a-directory-listing-sorted-by-creation-date-in-python
def getfiles(dirpath):
    '''esta funcion retorna la lista de archivos
       de un directorio ordenados por tiempo de
       creación'''
    a = [s for s in os.listdir(dirpath)
         if os.path.isfile(os.path.join(dirpath, s))]
    a.sort(key=lambda s: os.path.getmtime(os.path.join(dirpath, s)))
    return a


#Para hacer el fit con una gaussiana, definimos la funcion
def gauss_function(x, a, x0, sigma):
        return a*np.exp(-(x-x0)**2/(2*sigma**2))

#Fit de las curvas
def func(x, a, b, c):
    #((G*N*q*R)/(tau[i]-tau_s))*(np.exp(-t/tau_s)-np.exp(-t/tau[i])
    return a * (np.exp(-b * x) - np.exp(-c * x))
    #return a * np.exp(-b * x) + c

#xdata = np.linspace(0, 20*16, 17)
#y = func(t, 250, 1/0.3e3, 1/0.1e2)
#ydata = y + 0.2 * np.random.normal(size=len(xdata))

#popt, pcov = curve_fit(func, t, (ch2r/2**10)*1e3)

#plt.show()


#para hacer el fit, esto es lo más parecido
def vsal(x,a,b,c,x0):
        return -a*(np.exp(-b*(x-x0))-np.exp(-c*(x-x0)))

#x=np.linspace(0,1e3,1001)
#plt.plot(x,50+vsal(x,5500,0.02,0.018,1),'b')
#plt.plot(x,vsal(x,2000,0.02,0.01,1)-100,'b')

#tomado de http://blog.juliusschulz.de/blog/ultimate-ipython-notebook
#Reading large text files
#Reading text files using numpy is unfortunately not the fastest thing. The
#numpy.loadtxt() function loads the full file into the memory first, which makes
#it memory consuming and slow for large files. It also supports only one comment
#character, but the .xvg files I frequently use have to different beginning
#characters for non-data lines. I was also not satisfied by the other options
#provided by numpy, so here is what I came up with, to have a relatively
#convenient function for loading twodimensional large text files with numerical
#data:
def h_loadtxt(filename, ncols=None, dtyp=None, commentchars=["#","@"]):
    global data
    if ncols==None:
        with open(filename) as f:
            l = f.readline()
            while l[0] in commentchars:
                print(l)
                l = f.readline()
            ncols = len(l.split())
            data = np.fromfile(filename, sep=" ", dtype=dtyp)
    return data.reshape((len(data)/ncols, ncols))
