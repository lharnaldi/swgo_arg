# Home del Proyecto SWGO, filial Argentina #

<!--<img src="media/swgo_logo-textL.png" width="600" height="400">-->
![Image](media/swgo_logo-textL.png "Logo SWGO")

## ¿Qué es SWGO? ##

El potencial científico de un detector de rayos gamma en tierra de gran campo
de visión y un ciclo de trabajo muy alto ha sido demostrado por los instrumentos
de generación actual HAWC y ARGO y será ampliado en el hemisferio norte por
LHAASO. No existe tal instrumento en el hemisferio sur, donde hay un gran
potencial para el mapeo de emisiones a gran escala, así como para proporcionar
un completo acceso a fenómenos en el cielo, como objetos transitorios, variables en múltiples
longitudes de onda y mensajeros múltiples. El acceso a observaciones del Centro Galáctico y
en complemento con la instalación de CTA-South son motivaciones importantes
para un observatorio de rayos gamma en el sur. También hay un potencial
significativo para los estudios de rayos cósmicos, incluido su anisotropía.

El concepto básico para el futuro observatorio es el siguiente

* Un observatorio de rayos gamma basado en la detección de partículas a nivel
  del suelo, con un ciclo de trabajo cercano al 100% y un campo de visión
  del orden de estéreo-radianes.
* Ubicado en América del Sur a una latitud de entre 10 y 30 grados sur.
* A una altitud de 4.4 km o más.
* Cubriendo un rango de energía de cientos de GeV a cientos de TeV.
* Basado principalmente en unidades de detección Cherenkov en agua.
* Con un arreglo central compacto de detectores, de alto factor de llenado, con un área considerablemente
  más grande que HAWC y una sensibilidad significativamente mejor, más un arreglo externo de detectores dispersos.

Más información puede encontrarse en la
[Wiki](https://gitlab.com/lharnaldi/swgo_arg/-/wikis/home) de este proyecto o en la
[página del proyecto SWGO](https://www.swgo.org).

![Esquema SWGO](media/swgo_pressimages_detector_light_tank_small.jpg "Esquema del detector SWGO")***Figura 1. Esquema conceptual del detector SWGO***

