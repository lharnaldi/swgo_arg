import serial
import time
import datetime

#ser = serial.Serial(
#    port='/dev/ttyACM0',\
#    baudrate=9600,\
#    parity=serial.PARITY_NONE,\
#    stopbits=serial.STOPBITS_ONE,\
#    bytesize=serial.EIGHTBITS,\
#        timeout=0)
ser = serial.Serial('/dev/ttyACM0',9600)
ser.flushInput()


print("connected to: " + ser.portstr)

while True:
    try:
        line = ser.readline()
        #timestamp = str(time.time())
        timestamp = str(time.strftime('%Y-%m-%d,%H:%M:%S', time.localtime()))
        #timestamp = str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))
        with open('tsens_swgo.csv', 'a') as pyfile:
            pyfile.write(timestamp + ',' +line.decode() )
            print(timestamp + ',' + line.decode())
    except:
        print("Keyboard Interrupt")
        break


ser.close()
