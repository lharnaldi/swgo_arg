#!/bin/bash
temp=$(cat /sys/bus/w1/devices/28-3c01d0753ac5/w1_slave)
timestamp=$(date +%Y-%m-%d_%H:%M:%S)
echo $timestamp' '${temp: -5} >> /var/log/ds18b20.log
